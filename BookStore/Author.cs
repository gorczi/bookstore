﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore
{
    class Author
    {
        public Guid ID;
        public string Name;
        public string Surname;
        public uint YearOfBirth;
    }
}
