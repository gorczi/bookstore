﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore
{
    class IoHelper
    {
        public uint GetUintFromUser(string message)
        {
            uint result = 0;
            bool success = false;
            while (!success)
            {
                Console.Write(message);
                var input = Console.ReadLine();
                success = uint.TryParse(input, out result);
                if (!success)
                {
                    Console.WriteLine("It was not a number! Try again");
                }
            }
            return result;
        }
        public bool GetBoolFromUser(string message)
        {
            bool result = true;
            bool success = false;
            while (!success)
            {
                Console.Write(message);
                var input = Console.ReadLine();
                success = bool.TryParse(input, out result);
                if (!success)
                {
                    Console.WriteLine("It was not a bool! Try again");
                }
            }
            return result;
        }
        public double GetdoubleFromUser(string message)
        {
            double result = 0;
            bool success = false;
            while (!success)
            {
                Console.Write(message);
                var input = Console.ReadLine();
                success = double.TryParse(input, out result);
                if (!success)
                {
                    Console.WriteLine("It was not a double! Try again");
                }
            }
            return result;
        }
        public string GetStringFromUser(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }

    }
}
