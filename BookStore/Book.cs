﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookStore
{
    class Book
    {
        public Guid ID;
        public string Title;
        public uint Year;
        public Author Author;
        public double Price;
    }
}
