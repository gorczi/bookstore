﻿using System;
using System.Collections.Generic;


namespace BookStore
{
    class Program
    {
        private IoHelper _ioHelper;
        List<Author> _authors = new List<Author>();
        List<Book> _books = new List<Book>();

        public Program()
        {
            _ioHelper = new IoHelper();
        }

        static void Main(string[] args)
        {
            new Program().Run();
        }

        private void Run()
        {
            var exit = false;
            var functionsCount = 7;

            while (!exit)
            {
                Console.WriteLine("Choose option");
                Console.WriteLine("1. Add author");
                Console.WriteLine("2. Add book");
                Console.WriteLine("3. Print all books");
                Console.WriteLine("4. Print all books by author surname");
                Console.WriteLine("5. Sell book");
                Console.WriteLine("6. Print sold books");
                Console.WriteLine("7. Exit");

                var selection = int.Parse(Console.ReadLine());
                if (selection > 0 && selection <= functionsCount)
                {
                    switch (selection)
                    {
                        case 1:
                            AddAuthor();
                            break;
                        case 2:
                            AddBook();
                            break;
                        case 3:
                            PrintAllBooks();
                            break;
                        case 4:
                            PrintAllBooksByAuthor();
                            break;
                        case 5:
                            SellBook();
                            break;
                        case 6:
                            PrintSoldBooks();
                            break;
                        case 7:
                            exit = true;
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Wrong number! Try again.");
                }
            }

        }
        private void AddAuthor()
        {
            var newAuthor = new Author
            {
                ID = Guid.NewGuid(),
                Name = _ioHelper.GetStringFromUser("Enter author name: "),
                Surname = _ioHelper.GetStringFromUser("Enter author surname: "),
                YearOfBirth = _ioHelper.GetUintFromUser("Enter author year of birth")
            };
            _authors.Add(newAuthor);
        }

        private void AddBook()
        {
            var authorName = _ioHelper.GetStringFromUser("Enter author name: ");
            var authorSurname = _ioHelper.GetStringFromUser("Enter author surname: ");
            Author choosenAuthor = null;
            foreach (var author in _authors)
            {
                if (author.Name == authorName && author.Surname == authorSurname)
                {
                    choosenAuthor = author;
                }
            }
            if (choosenAuthor == null)
            {
                Console.WriteLine($"There is no user with specified name: {authorName} {authorSurname}");
                return;
            }

            var newBook = new Book
            {
                ID = Guid.NewGuid(),
                Title = _ioHelper.GetStringFromUser("Enter book title: "),
                Author = choosenAuthor,
                Price = _ioHelper.GetdoubleFromUser("Enter book price: "),
                Year = _ioHelper.GetUintFromUser("Enter book year: ")
            };
            _books.Add(newBook);
        }

        private void PrintAllBooks()
        {
            foreach (var book in _books)
            {
                Console.WriteLine($"{book.Author.Name} {book.Author.Surname} {book.Title} {book.Year} {book.Price.ToString("#.##")}");
            }
        }

        private void PrintAllBooksByAuthor()
        {
            var authorName = _ioHelper.GetStringFromUser("Enter author name: ");
            var authorSurname = _ioHelper.GetStringFromUser("Enter author surname: ");
            Author choosenAuthor = null;
            foreach (var author in _authors)
            {
                if (author.Name == authorName && author.Surname == authorSurname)
                {
                    choosenAuthor = author;
                }
            }
            if (choosenAuthor == null)
            {
                Console.WriteLine($"There is no user with specified name: {authorName} {authorSurname}");
                return;
            }
            Console.WriteLine($"Books of {choosenAuthor.Name} {choosenAuthor.Surname}");
            foreach (var book in _books)
            {
                if (book.Author == choosenAuthor)
                {
                    Console.WriteLine($"{book.Author.Name} {book.Author.Surname} {book.Title} {book.Year} {book.Price.ToString("#.##")}");
                }
            }

        }


        private void PrintSoldBooks()
        {
        }

        private void SellBook()
        {
        }

    }
}